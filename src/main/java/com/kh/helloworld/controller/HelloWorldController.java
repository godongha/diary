package com.kh.helloworld.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class HelloWorldController {

	@RequestMapping("/helloWorld1")
	@GetMapping("/helloWorld")
	public ModelAndView helloWorld(ModelAndView modelAndView) {

		modelAndView.setViewName("helloWorld");
		modelAndView.addObject("hello", "안녕");

		return modelAndView;
	}
}
